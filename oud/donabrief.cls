%
% Copyright (C) 2012-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.%%%%%%
%
%% $Id: donabrief.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{donabrief}[2012/01/19 v0.1 Donateursbrief template]

\LoadClass[12pt]{aesbrief} %Laad standaard brief class

\RequirePackage{aes}
\RequirePackage{ifthen}

%%%%
% INIT
%%%%

\welkleur
\cienaam{Secretariaat}
\email{secretaris@a-eskwadraat.nl}
\subject{Donateursbrief}

\newboolean{maakBriefOokAlsErGeenBijlagenZijn}
\newboolean{alma} 	\newboolean{toonalma}
\newboolean{avmail} 	\newboolean{toonav}
\newboolean{avpost} 	
\newboolean{jaarverslag}\newboolean{toonjaarverslag}
\newboolean{reisverslag}\newboolean{toonreisverslag}
\newboolean{bonus} 	\newboolean{toonbonus}
\newboolean{vakid} 	\newboolean{toonvakid}
\newboolean{toonbrief} %\setboolean{toonbrief}{true}

%%Bij deze donateursbrief: %%%%%%%%%%%%%%%%%%%%

\setboolean{toonalma}{false}
\setboolean{toonav}{false}
\setboolean{toonjaarverslag}{false}
\setboolean{toonreisverslag}{false}
\setboolean{toonbonus}{false}
\setboolean{toonvakid}{false}

\setboolean{maakBriefOokAlsErGeenBijlagenZijn}{true}

%%Deze geheel weglaten als er geen algemene bijlage is
%\newcommand{\extraBijlage}{ Constitutiekaartje }

\newcounter{bijlagenTeller}

\newcommand{\hetonderwerp}{}
\newboolean{meeronderwerpen}
\setboolean{meeronderwerpen}{FALSE}

\newcommand{\geefonderwerp}{
  \ifvakid \expandafter\def\expandafter\hetonderwerp\expandafter{\hetonderwerp { }vakidioot} \setboolean{meeronderwerpen}{true} \fi
  \ifreisverslag \expandafter\def\expandafter\hetonderwerp\expandafter{\hetonderwerp {\ifmeeronderwerpen , \fi } reisverslag \setboolean{meeronderwerpen}{true}}  \fi
  \ifjaarverslag \expandafter\def\expandafter\hetonderwerp\expandafter{\hetonderwerp {\ifmeeronderwerpen , \fi } jaarverslag \setboolean{meeronderwerpen}{true}}  \fi
  \ifalma \expandafter\def\expandafter\hetonderwerp\expandafter{\hetonderwerp {\ifmeeronderwerpen , \fi } almanak \setboolean{meeronderwerpen}{true}}  \fi
  \ifavpost \expandafter\def\expandafter\hetonderwerp\expandafter{\hetonderwerp {\ifmeeronderwerpen , \fi } AV-notulen \setboolean{meeronderwerpen}{true}}  \fi
  \subject{\hetonderwerp}
  %\ifvakid \ifreisverslag \subject{Vakidioot en reisverslag Amerika} \fi \fi
}

\newsavebox{\inlbox}
\newcommand{\inleiding}[1]{\sbox{\inlbox}{\parbox{\textwidth}{#1}}}

\newsavebox{\afslbox}
\newcommand{\afsluiting}[1]{\sbox{\afslbox}{\parbox{\textwidth}{#1}}}

\newsavebox{\vidbox}
\newcommand{\vakidioot}[1]{\sbox{\vidbox}{\parbox{\textwidth}{#1}}}

\newsavebox{\reisbox}
\newcommand{\reis}[1]{\sbox{\reisbox}{\parbox{\textwidth}{#1}}}

\newsavebox{\almabox}
\newcommand{\almanak}[1]{\sbox{\almabox}{\parbox{\textwidth}{#1}}}

\newsavebox{\jvsbox}
\newcommand{\jvs}[1]{\sbox{\jvsbox}{\parbox{\textwidth}{#1}}}

\newsavebox{\bonusbox}
\newcommand{\bonusding}[1]{\sbox{\bonusbox}{\parbox{\textwidth}{#1}}}

\def\almahier{
\ifalma
\usebox{\almabox}
\fi
}

\def\vakidhier{
\ifvakid
\usebox{\vidbox}
\fi
}

\def\reishier{
\ifreisverslag
\usebox{\reisbox}
\fi
}

\def\jvshier{
\ifjaarverslag
\usebox{\jvsbox}
\fi
}

\def\bonushier{
\ifbonus
\usebox{\bonusbox}
\fi
}

\long\def\briefformat{
\vakidhier\par\reisverslaghier
}

\long\def\formateer#1{
\long\def\briefformat{#1}
}



% donateur{naam\\straat~nr\\postcode~~plaats}{aanhef}{alma}{avnotulen}{jaarverslag}{studiereis}{bonus}{vakid}
\newcommand{\donateur}[8]{
  %zet alle parameters
  \setboolean{alma}{#3}
  \ifthenelse{\equal{#4}{email}}{ \setboolean{avmail}{true} }{ \setboolean{avmail}{false} }
  \ifthenelse{\equal{#4}{post}}{ \setboolean{avpost}{true} }{ \setboolean{avpost}{false} }
  \setboolean{jaarverslag}{#5}
  \setboolean{reisverslag}{#6}
  \setboolean{bonus}{#7}
  \setboolean{vakid}{#8}
  
  %Kijk of er bijlagen meegegeven worden
  \setcounter{bijlagenTeller}{0}

  %is er een algemene bijlage?
  \ifx\extraBijlage\undefined
  \else
    \stepcounter{bijlagenTeller}
  \fi

  %zijn er extra bijlages?
  \iftoonalma		\ifalma 	\stepcounter{bijlagenTeller}\fi\fi
  \iftoonav		\ifavpost	\stepcounter{bijlagenTeller}\fi\fi
  \iftoonjaarverslag	\ifjaarverslag 	\stepcounter{bijlagenTeller}\fi\fi
  \iftoonreisverslag	\ifreisverslag 	\stepcounter{bijlagenTeller}\fi\fi
  \iftoonbonus		\ifbonus	\stepcounter{bijlagenTeller}\fi\fi
  \iftoonvakid		\ifvakid	\stepcounter{bijlagenTeller}\fi\fi

  % Onderwerpen
  \geefonderwerp
 
  % kijk of de brief getoond moet worden
  \setboolean{toonbrief}{false}
  \ifmaakBriefOokAlsErGeenBijlagenZijn \setboolean{toonbrief}{true} \fi

  % Bijlages, alleen als ze er zijn ;)
  \ifthenelse{\thebijlagenTeller > 0}
  {
    \setboolean{toonbrief}{true}
    \ifthenelse{\thebijlagenTeller > 1}
    { 
      \bijlagen{
	\ifx\extraBijlage\undefined \else \item \extraBijlage \fi
	\iftoonalma	\ifalma 	\item Almanak 	\fi\fi
	\iftoonav		\ifavpost	\item AV-notulen 	\fi\fi
	\iftoonjaarverslag\ifjaarverslag	\item Jaarverslag	\fi\fi
	\iftoonreisverslag\ifreisverslag 	\item Reisverslag \fi\fi
	\iftoonbonus	\ifbonus	\item Bonus 	\fi\fi
	\iftoonvakid	\ifvakid	\item Vakidioot 	\fi\fi
      }
    }{
      \bijlage{
	\ifx\extraBijlage\undefined \else \extraBijlage \fi
	\iftoonalma	\ifalma 	Almanak 	\fi\fi
	\iftoonav		\ifavpost	AV-notulen 	\fi\fi
	\iftoonjaarverslag\ifjaarverslag	Jaarverslag	\fi\fi
	\iftoonreisverslag\ifreisverslag 	Reisverslag \fi\fi
	\iftoonbonus	\ifbonus	Bonus 	\fi\fi
	\iftoonvakid	\ifvakid	Vakidioot 	\fi\fi
      }
    }
  }{}



\iftoonbrief

% start de brief %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{brief}{#1}

\opening{Beste #2,}

\usebox{\inlbox}


\briefformat


% \ifreisverslag
% \usebox{\reisbox}
% \fi
% 
% 
% \ifvakid
% \usebox{\vidbox}
% \fi

\usebox{\afslbox}


\closing{Met vriendelijke groet,}

\end{brief}
\setboolean{meeronderwerpen}{false}
\renewcommand{\hetonderwerp}{}
 \fi

} %einde van de donateurmacro