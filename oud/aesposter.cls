% Copyright (C) 2006-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: aesposter.cls 733 2015-02-23 14:38:43Z aldo $
%% A-Eskwadraat poster class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHORS
%%%%%%%%%%%%%%%
%% Wouter Duivesteijn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TODO:
%%%%%%%%%%%%%%%
%%  - Layouts maken
%%  - Layouts customisifcationisizerable maken
%%  - Meerdere talen inplementeren
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Identification, loading packages and base class, modifying unit length etc.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{aesposter}[2005/11/17 v0.85 by Wouter Duivesteijn]

\LoadClass{article}

\RequirePackage[dutch]{babel}
\RequirePackage[pdftex]{graphicx}
\RequirePackage{verbatim}
\RequirePackage{color}
\RequirePackage{aes}
\RequirePackage{uusol}
\RequirePackage{multicol}
\RequirePackage{ifthen}

\setlength{\unitlength}{1mm}

\setlength{\parindent}{0mm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page style parameters

\setlength{\pdfpagewidth}{210mm}
\setlength{\pdfpageheight}{297mm}

\setlength{\hoffset}{-1in}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textwidth}{210mm}

\setlength{\voffset}{-1in}
\setlength{\topmargin}{0mm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}
\setlength{\topskip}{0mm}
\setlength{\textheight}{297mm}
\setlength{\footskip}{0mm}

\pagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definieer de kleuren.

\definecolor{AESGeel}{rgb}{1,0.85,0}
\definecolor{AESRood}{rgb}{0.59,0,0.12}

\pagecolor{AESGeel}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Maak nuttige commando's. Eerst de interne commando's die in eerste instantie
% leeg zijn, ...

\newcommand{\AEP@weken}{}
\newcommand{\AEP@acteenlayout}{}
\newcommand{\AEP@acteenfotoeen}{}
\newcommand{\AEP@acteenfototwee}{}
\newcommand{\AEP@acteenplaats}{}
\newcommand{\AEP@acteendata}{}
\newcommand{\AEP@acteencie}{}
\newcommand{\AEP@acteenkop}{}
\newcommand{\AEP@acteentekst}{}
\newcommand{\AEP@acttweelayout}{}
\newcommand{\AEP@acttweefotoeen}{}
\newcommand{\AEP@acttweefototwee}{}
\newcommand{\AEP@acttweeplaats}{}
\newcommand{\AEP@acttweedata}{}
\newcommand{\AEP@acttweecie}{}
\newcommand{\AEP@acttweekop}{}
\newcommand{\AEP@acttweetekst}{}
\newcommand{\AEP@actdrielayout}{}
\newcommand{\AEP@actdriefotoeen}{}
\newcommand{\AEP@actdriefototwee}{}
\newcommand{\AEP@actdrieplaats}{}
\newcommand{\AEP@actdriedata}{}
\newcommand{\AEP@actdriecie}{}
\newcommand{\AEP@actdriekop}{}
\newcommand{\AEP@actdrietekst}{}

% ... dan het interne commando dat niet leeg is, en de layout kiest, ...
\newcommand{\activiteit}[8]{
	% Als er maar een foto is...
	\ifthenelse{\equal{#3}{}}{
		% kies dan een van de volgende layouts...
		
		% Standaard
		\ifthenelse{\equal{#1}{standaard}}{

			\begin{minipage}[c][32mm][c]{60mm}
				\includegraphics[width=60mm]{#2}
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][55mm][c]{75mm}
				\begin{Large}\textbf{#7}\end{Large}\\[5mm]
					#8
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][55mm][c]{30mm}
				\begin{tabular}{|l|}
				\hline\\
				#4\\
				#5\\
				#6\\ \\
				\hline
				\end{tabular}
			\end{minipage}			

		}{
		
		\ifthenelse{\equal{#1}{standaardfr}}{

			\begin{minipage}[c][55mm][c]{30mm}
				\begin{tabular}{|l|}
				\hline\\
				#4\\
				#5\\
				#6\\ \\
				\hline
				\end{tabular}
			\end{minipage}			
			\hspace{5mm}
			\begin{minipage}[c][55mm][c]{75mm}
				\begin{Large}\textbf{#7}\end{Large}\\[5mm]
					#8
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][32mm][c]{60mm}
				\includegraphics[width=60mm]{#2}
			\end{minipage}

		}{

		% Tweekoloms
		\ifthenelse{\equal{#1}{tweekoloms}}{

			\begin{center}
				\begin{Large}
					\textbf{#7}
				\end{Large}
			\end{center}
			\begin{minipage}[c][45mm][c]{180mm}
				\begin{minipage}[c][45mm][c]{75mm}
					\setlength\columnseprule{0.3mm}
					\begin{multicols}{2}
					#8
					\end{multicols}
				\end{minipage}
				\hspace{5mm}
				\begin{minipage}[c][32mm][c]{60mm}
					\includegraphics[width=60mm]{#2}
				\end{minipage}
				\hspace{5mm}
				\begin{minipage}[c][45mm][c]{30mm}
					\begin{tabular}{|l|}
						\hline\\
						#4\\
						#5\\
						#6\\ \\
						\hline
					\end{tabular}
				\end{minipage}
			\end{minipage}
		
		}{
		
		}}}
		
	}{	% en als er twee foto's zijn een van de volgende.
		
		% De liggende foto rechts
		\ifthenelse{\equal{#1}{liggendrechts}}{
		
			\begin{minipage}[c][40mm][c]{40mm}
				\includegraphics[width=40mm]{#2}
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][55mm][t]{63mm}
				\begin{center}
					#4\hspace{20mm}\mbox{}\\
					#5\\
					\mbox{}\hspace{20mm}#6
				\end{center}
				\begin{Large}\textbf{#7}\end{Large}\\[2mm]
					#8
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][32mm][c]{60mm}
				\includegraphics[width=60mm]{#3}
			\end{minipage}
		
		}{
		
		% De liggende foto links
		\ifthenelse{\equal{#1}{liggendlinks}}{
		
			\begin{minipage}[c][32mm][c]{60mm}
				\includegraphics[width=60mm]{#2}
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][55mm][t]{63mm}
				\begin{center}
					#4\hspace{20mm}\mbox{}\\
					#5\\
					\mbox{}\hspace{20mm}#6
				\end{center}
				\begin{Large}\textbf{#7}\end{Large}\\[2mm]
					#8
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][40mm][c]{40mm}
				\includegraphics[width=40mm]{#3}
			\end{minipage}
		}{
		
		% Twee staande foto's
		\ifthenelse{\equal{#1}{beidestaand}}{
		
			\begin{minipage}[c][40mm][c]{40mm}
				\includegraphics[width=40mm]{#2}
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][55mm][t]{83mm}
				\begin{center}
					#4\hspace{20mm}\mbox{}\\
					#5\\
					\mbox{}\hspace{20mm}#6
				\end{center}
				\begin{Large}\textbf{#7}\end{Large}\\[2mm]
					#8
			\end{minipage}
			\hspace{5mm}
			\begin{minipage}[c][40mm][c]{40mm}
				\includegraphics[width=40mm]{#3}
			\end{minipage}
		
		}{
		
		}}}
	
	}
}

% ... en tenslotte de interfacecommando's. Dat zijn er dus maar vier. Easy!

\newcommand{\maakweken}[1]{\renewcommand{\AEP@weken}{#1}}
\newcommand{\maakactiviteiteen}[8]{
	\renewcommand{\AEP@acteenlayout}{#1}
	\renewcommand{\AEP@acteenfotoeen}{#2}
	\renewcommand{\AEP@acteenfototwee}{#3}
	\renewcommand{\AEP@acteenplaats}{#4}
	\renewcommand{\AEP@acteendata}{#5}
	\renewcommand{\AEP@acteencie}{#6}
	\renewcommand{\AEP@acteenkop}{#7}
	\renewcommand{\AEP@acteentekst}{#8}
}
\newcommand{\maakactiviteittwee}[8]{
	\renewcommand{\AEP@acttweelayout}{#1}
	\renewcommand{\AEP@acttweefotoeen}{#2}
	\renewcommand{\AEP@acttweefototwee}{#3}
	\renewcommand{\AEP@acttweeplaats}{#4}
	\renewcommand{\AEP@acttweedata}{#5}
	\renewcommand{\AEP@acttweecie}{#6}
	\renewcommand{\AEP@acttweekop}{#7}
	\renewcommand{\AEP@acttweetekst}{#8}
}
\newcommand{\maakactiviteitdrie}[8]{
	\renewcommand{\AEP@actdrielayout}{#1}
	\renewcommand{\AEP@actdriefotoeen}{#2}
	\renewcommand{\AEP@actdriefototwee}{#3}
	\renewcommand{\AEP@actdrieplaats}{#4}
	\renewcommand{\AEP@actdriedata}{#5}
	\renewcommand{\AEP@actdriecie}{#6}
	\renewcommand{\AEP@actdriekop}{#7}
	\renewcommand{\AEP@actdrietekst}{#8}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dan moet de poster nog gemaakt worden. De gebruiker zal dus nog een 
% vijfde commando moeten geven. Vermoeiend...

\newcommand{\maakposter}{
	\vspace*{10mm}
% De kop
	\fontfamily{cmss}
	\fontseries{sbc}
	\selectfont
	\begin{center}
	\textcolor{AESRood}{\resizebox{!}{20mm}{It just happened}}
	\end{center}
	\begin{center}
	\resizebox{!}{10mm}{Activiteiten week \AEP@weken}
	\end{center}
	\normalfont

% De eerste activiteit
	\vspace{1mm}
	\begin{center}
		\begin{minipage}[c][60mm][c]{180mm}
			\activiteit{\AEP@acteenlayout}{\AEP@acteenfotoeen}{\AEP@acteenfototwee}{\AEP@acteenplaats}{
				\AEP@acteendata}{\AEP@acteencie}{\AEP@acteenkop}{\AEP@acteentekst}
		\end{minipage}
	\end{center}

% De tweede activiteit
	\vspace{2mm}
	\begin{center}
		\begin{minipage}[c][60mm][c]{180mm}
			\activiteit{\AEP@acttweelayout}{\AEP@acttweefotoeen}{\AEP@acttweefototwee}{\AEP@acttweeplaats}{
				\AEP@acttweedata}{\AEP@acttweecie}{\AEP@acttweekop}{\AEP@acttweetekst}
		\end{minipage}
	\end{center}

% De derde activiteit
	\vspace{2mm}
	\begin{center}
		\begin{minipage}[c][60mm][c]{180mm}
			\activiteit{\AEP@actdrielayout}{\AEP@actdriefotoeen}{\AEP@actdriefototwee}{\AEP@actdrieplaats}{
				\AEP@actdriedata}{\AEP@actdriecie}{\AEP@actdriekop}{\AEP@actdrietekst}
		\end{minipage}
	\end{center}

% De footer
	\vfill
	\hspace{10mm}
	\begin{minipage}[b][20mm][b]{37mm}
		\includegraphics[height=20mm]{aes2-kleur.pdf}
	\end{minipage}
	\hfill
	\begin{minipage}[b][18mm][t]{70mm}
		\resizebox{!}{10mm}{\uusol{11}}
	\end{minipage}
	\hspace{10mm}
	\vspace{10mm}
}
