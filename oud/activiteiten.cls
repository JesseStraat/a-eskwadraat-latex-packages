% Copyright (C) 2007-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: activiteiten.cls 807 2017-04-04 10:53:24Z aldo $

%Werking en nut: Maakt activiteitenposters, volledig in onbruik geraakt

\NeedsTeXFormat{LaTeX2e}
\newcommand{\act@classname}{activiteiten}
\ProvidesClass{\act@classname}[2010/09/21 v1.1 Activiteitenposter (Sweitse)]

\LoadClass[a4paper,12pt,oneside]{article}

%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Setting TAD %%%%%
%%%%%%%%%%%%%%%%%%%%%%%

%% User command: \kleur[bgcolor]{fgcolor}
%% Define the color to be used for header and footer,
%% and optionally set a background color.
\newcommand{\kleur}[2][white]{%
  \makeatletter
  \newcommand{\act@fgcolor}{#2}
  \newcommand{\act@bgcolor}{#1}
  \makeatother
}

%% User command: \achtergrond{bgimage}
%% Use this image as wallpaper.
\newcommand{\achtergrond}[1]{%
  \makeatletter
  \newcommand{\act@background}{#1}
  \makeatother
}

\RequirePackage{ifthen,wallpaper}

\AtBeginDocument{%
  % Test for obligatory use of command \kleur in preamble
  \ifthenelse{\isundefined{\act@fgcolor}}%
    {\ClassError{\act@classname}{No textcolor defined}%
      {Please put the command \kleur[<bgcolor>]{<textcolor>} in the preamble of your document}}
    {}
  % Include background image with ``hoekje'' if defined
  \ifthenelse{\isundefined{\act@background}}%
    {\ClassInfo{\act@classname}{No background image defined}\pagecolor{\act@bgcolor}}
    {\TileWallPaper{210mm}{297mm}{\act@background}%
    \TileWallPaper{210mm}{297mm}{aeshoekje.pdf}}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Main commands %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

%% User command
%% Replaces section, used to display the name of the month.
\newcommand{\maand}[3][.75\textwidth]{%
  \centerline{\resizebox{#1}{!}{\maandfont\textcolor{#2}{#3}}}%
  \bigskip%
}

%% User command
%% Arguments: #1:position #2:date #3:day #4:act #5:place
\newcommand{\activiteit}[1][a]{%
  \par
  \ifx#1r
    \let\next\act@x
  \else
    \ifx#1l
      \let\next\act@y
    \else
      \ifodd\theact@nr
        \let\next\act@x
      \else
	    \let\next\act@y
      \fi
	\fi
  \fi
  \next
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Auxilary commands %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{act@nr}

\def\act@x#1#2#3#4{%
  \setcounter{act@nr}{0}
  {\actbox{\act@primary}{\act@secondary}{#3}{#4}\hspace{2mm}%
    \datebox{\act@secondary}{\act@primary}{#1}{#2}%
  }%
}

\def\act@y#1#2#3#4{%
  \setcounter{act@nr}{1}
  {\datebox{\act@secondary}{\act@primary}{#1}{#2}~\hspace{2mm}%
    \actbox{\act@primary}{\act@secondary}{#3}{#4}%
  }%
}

\newlength{\act@tmpwidth}
\newlength{\act@tmpheight}
\newlength{\act@maxwidth}

\newcommand{\datebox}[4]{%
  \newcommand{\act@currentcontent}{\actfont\Huge\textcolor{#2}{#3}}%
  \settowidth{\act@tmpwidth}{\act@currentcontent}%
  \settoheight{\act@tmpheight}{\act@currentcontent}%
  \setlength{\act@maxwidth}{15mm}%
  %
  \newcommand{\act@currentfitcontent}{%
    \ifthenelse{\equal{#4}{}}{\addtolength{\act@maxwidth}{4mm}}{}%
    \ifthenelse{\lengthtest{\act@tmpwidth>\act@maxwidth}}%
      {\resizebox{\act@maxwidth}{!}{\act@currentcontent}}%
      {\act@currentcontent}%
  }
  \ifthenelse{\equal{#4}{}}%
    {\colorbox{#1}{\makebox[20mm][c]{\Huge\rule{0mm}{\act@tmpheight}\act@currentfitcontent}}}%
    {\colorbox{#1}{\makebox[20mm]{\rotatebox{90}{\actfont\Large\textcolor{#2}{\textsc{#4}}}\hfill\act@currentfitcontent}}}%
}

\newcommand{\actbox}[4]{%
  \colorbox{#1}{%
    \parbox[t]{170mm}{\raggedright%
      \actfont\Huge\textcolor{#2}{#3}%
      \hspace*{\stretch{1}}\actfont\Large\textcolor{#2}{\textsc{#4}}%
    }%
  }%
}


%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Font commands %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

\input{Konanur.fd}

\newcommand{\act@font@maand}{augie}
\newcommand{\act@font@act}{phv}

\newcommand{\kopfont}{\usefont{U}{Konanur}{xl}{n}}
\newcommand{\maandfont}{\makeatletter\fontfamily{\act@font@maand}\selectfont\makeatother}
\newcommand{\actfont}{\makeatletter\fontfamily{\act@font@act}\selectfont\makeatother}

\renewcommand{\large}{\fontsize{4mm}{6mm}\selectfont}
\renewcommand{\Large}{\fontsize{6mm}{8mm}\selectfont}
\renewcommand{\LARGE}{\fontsize{8mm}{10mm}\selectfont}
\renewcommand{\huge}{\fontsize{10mm}{12mm}\selectfont}
\renewcommand{\Huge}{\fontsize{12mm}{15mm}\selectfont}
\newcommand{\HUGE}{\fontsize{14mm}{16mm}\selectfont}


%%%%%%%%%%%%%%%%%%%%%
%%%%% Pagestyle %%%%%
%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{url}

\newcommand{\act@titlename}{Activiteiten}
\newcommand{\act@url}{\url{www.a-eskwadraat.nl/activiteiten}}

\newlength{\act@foottmp}
\settowidth{\act@foottmp}{\Huge\act@url}

\renewcommand{\ps@plain}{%
  \renewcommand{\@oddhead}{\centerline{\resizebox{\textwidth}{!}{\kopfont\textcolor{\act@fgcolor}{\act@titlename}}}}%
  \renewcommand{\@oddfoot}{%
    \parbox[b]{\act@foottmp}{\Huge\actfont\color{\act@fgcolor}\act@url}
  }%
}

\renewcommand{\ps@empty}{%
  \renewcommand{\@oddhead}{\centerline{\resizebox{\textwidth}{!}{\kopfont\textcolor{\act@fgcolor}{\act@titlename}}}}%
  \renewcommand{\@oddfoot}{\centerline{\raisebox{-5mm}[0mm]{\act@logo}}}%
}

% Set default pagestyle
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Color settings %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{color}
\RequirePackage[cmyk]{xcolor}

%% Default text color of activities
\newcommand{\act@primary}{rood}
%% Default box color of activities
\newcommand{\act@secondary}{wit}

%% User command: set primary and secondary colors
%% Changes [box] and {text} colors
\newcommand{\actkleur}[2][white]{%
  \makeatletter
  \renewcommand{\act@primary}{#2}
  \renewcommand{\act@secondary}{#1}
  \makeatother
}

%% Define standard colors from color.sty in Dutch:
\definecolor{zwart}{named}{black}
\definecolor{wit}{named}{white}
%\definecolor{rood}{named}{red}
%\definecolor{groen}{named}{green}
%\definecolor{blauw}{named}{blue}
\definecolor{cyaan}{named}{cyan}
\definecolor{magenta}{named}{magenta}
\definecolor{geel}{named}{yellow}

%% Corporate colors
\definecolor{UUgeel}		{cmyk}{0.00, 0.115,0.94, 0.15}
\definecolor{UUrood}		{cmyk}{0.00, 0.91, 0.76, 0.06}

%% Additional corporate colors
\definecolor{oranje}		{cmyk}{0.00, 0.30, 0.90, 0.00}
\definecolor{rood}		{cmyk}{0.20, 1.00, 0.90, 0.10}
\definecolor{groen}		{cmyk}{0.90, 0.15, 0.70, 0.00}
\definecolor{blauw}		{cmyk}{0.90, 0.55, 0.00, 0.00}
\definecolor{paars}		{cmyk}{0.70, 1.00, 0.20, 0.05}

%% Secondary colors
\definecolor{donkerrood}	{cmyk}{0.30, 1.00, 0.90, 0.30}
\definecolor{terra}		{cmyk}{0.01, 0.65, 0.97, 0.13}
\definecolor{lichtgeel}		{cmyk}{0.00, 0.05, 1.00, 0.00}
\definecolor{donkergroen}	{cmyk}{1.00, 0.30, 0.80, 0.20}
\definecolor{olijfgroen}	{cmyk}{0.20, 0.05, 1.00, 0.00}
\definecolor{lichtgroen}	{cmyk}{0.10, 0.00, 1.00, 0.00}
\definecolor{donkerblauw}	{cmyk}{1.00, 0.90, 0.20, 0.10}
\definecolor{azuurblauw}	{cmyk}{1.00, 0.25, 0.20, 0.00}
\definecolor{lichtblauw}	{cmyk}{0.60, 0.00, 0.10, 0.00}
\definecolor{donkerpaars}	{cmyk}{0.70, 0.90, 0.50, 0.30}
\definecolor{diepdonkerpaars}	{cmyk}{0.83, 1.00, 0.63, 0.00}
\definecolor{fuchsia}		{cmyk}{0.06, 1.00, 0.10, 0.00}
\definecolor{donkerfuchsia}	{cmyk}{0.30, 1.00, 0.40, 0.05}

%% Faculty of Science
\definecolor{zilver}		{gray}{0.50}
\definecolor{betawetenschappen}	{cmyk}{0.00, 0.35, 0.90, 0.00}
\definecolor{biologie}		{named}{groen}
\definecolor{informatica}	{named}{rood}
\definecolor{farmacie}		{named}{azuurblauw}
\definecolor{natuurkunde}	{named}{paars}
\definecolor{scheikunde}	{named}{blauw}
\definecolor{wiskunde}		{cmyk}{0.35, 0.94, 0.27, 0.13}

%% For debug and users: display a color palette
\newcommand{\displaypalette}{%
  \section*{\textcolor{\act@fgcolor}{Color Palette}}
  \newcommand{\cc}[1]{\raisebox{4pt}{\colorbox{##1}{\mbox{\hspace{12pt}}}}&\textcolor{\act@fgcolor}{##1}}
  \begin{tabular}{*5{cl}}
  \cc{zwart}	& \cc{oranje}	& \cc{lichtgeel}	& \cc{lichtblauw}	& \cc{betawetenschappen}\\
  \cc{zilver}	& \cc{rood}	& \cc{donkergroen}	& \cc{donkerpaars}	& \cc{biologie}			\\
  \cc{wit}	& \cc{groen}	& \cc{olijfgroen}	& \cc{diepdonkerpaars}	& \cc{farmacie}			\\
  \cc{cyaan}	& \cc{blauw}	& \cc{lichtgroen}	& \cc{fuchsia}		& \cc{informatica}		\\
  \cc{magenta}	& \cc{paars}	& \cc{donkerblauw}	& \cc{donkerfuchsia}	& \cc{natuurkunde}		\\
  \cc{geel}	& \cc{donkerrood}& \cc{azuurblauw}	& \cc{UUgeel}		& \cc{scheikunde}		\\
  &		& \cc{terra}	& \cc{lichtblauw}	& \cc{UUrood}		& \cc{wiskunde}			\\
  \end{tabular}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Setting page dimensions %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{calc}

%% Pagesize
\setlength{\pdfpagewidth}	{210mm}
\setlength{\pdfpageheight}	{297mm}

%% Vertical content
\setlength{\topmargin}		{3.5mm-1in}
\setlength{\headheight}		{ 20mm}
\setlength{\headsep}		{ 10mm}
\setlength{\textheight}		{215mm}
\setlength{\footskip}		{ 40mm}

%% Other dimensions
\setlength{\textwidth}		{203mm}
\setlength{\oddsidemargin}	{3.5mm-1in}
\setlength{\parskip}		{  2mm}
\setlength{\parindent}		{  0mm}
\setlength{\fboxsep}		{  2mm}
\setlength{\fboxrule}		{  0mm}
\setlength{\unitlength}		{  1mm}
