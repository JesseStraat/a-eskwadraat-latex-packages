% Copyright (C) 2001-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: ibaverklaart.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ibaverklaart}[2001/05/14 v1.0 IBA Verklaart (en weet het beter)]
%
\LoadClass[oneside,a4paper,12pt]{article}
%
\RequirePackage[dutch]{babel}
\RequirePackage{graphicx,calc,wrapfig}
\RequirePackage[utf8]{inputenc}
\RequirePackage{aes}

% het lettertype is overal Helvetica
\renewcommand\sffamily{phv}
\renewcommand\familydefault\sffamily
%
%
%
% Margins ed
\paperheight=297mm
\voffset=-1in
\topmargin=5mm
\headheight=0pt
\headsep=0pt
\footskip=0pt
\newdimen\bottommargin \bottommargin=20mm
\setlength\textheight{\paperheight%
  -\topmargin-\headheight-\headsep-\footskip-\bottommargin}
%
\paperwidth=210mm
\hoffset=-1in
\oddsidemargin=22mm
\evensidemargin=\oddsidemargin
\setlength\textwidth{\paperwidth-\oddsidemargin-\evensidemargin}
%
\parindent=0pt
\parskip=8pt plus2pt minus2pt
%
\fboxsep=-\fboxrule
%
%
% Foots en headers
\def\@evenfoot{\hss%
   \ifnum\count0>1%
   \ClassWarning{ibaverklaart}%
        {Deze `IBA verklaart' is groter dan 1 pagina!}%
        {`IBA verklaart's moeten kort zijn, anders worden ze niet gebruikt.}%
   \fi%
}
\let\@oddfoot\@evenfoot
%
%
% Het `IBA verklaart' logo
\newbox\IBA@logo
\setbox\IBA@logo\hbox{%
  \rotatebox{-15}{%
    \includegraphics[width=3.5cm]{ibaverklaart}
    \raise7mm\vbox{\huge\fontfamily{phv}\selectfont%
      \moveright2mm\hbox{\textbf{IBA}}
      \moveleft1cm\hbox{\textit{verklaart}}
    }}
}
\wd\IBA@logo=50mm
\ht\IBA@logo=60pt
\dp\IBA@logo=-\ht\IBA@logo
%
%
% De titel
\def\IBA@titel{}
\def\titel#1{\def\IBA@titel{#1}}
%
%
% De kop met het logo
\def\IBA@kop{%
  \null{\huge\bfseries \IBA@titel}%
  \hfill\box\IBA@logo\null\\[20pt]%
}
%
\AtBeginDocument{%
  \fontfamily{phv}\selectfont%
  \IBA@kop}
%
%
% Commando's voor users:
\def\lijntje{\bigskip \hrule width\textwidth \medskip}
%
%
\endinput
