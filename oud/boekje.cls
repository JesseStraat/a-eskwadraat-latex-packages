% Copyright (C) 2006-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: boekje.cls 733 2015-02-23 14:38:43Z aldo $


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{boekje}[2005/12/14 v0.5 A-Eskwadraat boekje class]
\RequirePackage[utf8]{inputenc}

\newif\ifeng@art \eng@artfalse
\newif\ifnumbering@art \numbering@artfalse
\DeclareOption{english}{\eng@arttrue}
\DeclareOption{nummering}{\numbering@arttrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions

\ifeng@art\RequirePackage[english]{babel}
\else\RequirePackage[dutch]{babel}
\fi
\LoadClass[a5paper]{book}
\RequirePackage{array,fancyhdr,tabularx,times,pifont,aes,wallpaper,graphicx}

% bladspiegel
\setlength{\evensidemargin}{16mm}
\setlength{\oddsidemargin}{23mm}
\setlength{\hoffset}{-1in} %standaard is 1 inch.
\setlength{\textwidth}{171mm} %= 210mm-16mm-23mm
\setlength{\topmargin}{10mm}
\setlength{\voffset}{-1in}
\setlength{\textheight}{25cm}
\setlength{\headheight}{20pt}
\setlength{\headsep}{4mm}
\setlength{\marginparwidth}{45pt}
\setlength{\marginparsep}{11pt}
\setlength{\footskip}{36pt}
\setlength{\marginparpush}{5pt}

\setlength{\parindent}{0pt}
\addtolength{\itemsep}{-\parskip}

%%%
%%% KOPJES
%%%
\ifnumbering@art\setcounter{secnumdepth}{0}
\else\setcounter{secnumdepth}{-3}\fi
\setcounter{tocdepth}{1}

% Layout
\def\aes@achtergrond{a-es2.pdf} % pdflatex all the way
\def\achtergrond#1{\global\def\aes@achtergrond{#1}}
\def\aes@commissie{%
  \ClassWarning{boekje}{Geen \protect\commissie\space gevonden.\MessageBreak
  Gebruik \protect\commissie{TeXnicie}\space om het in te stellen \MessageBreak
  Standaard (lege) commissienaam ingevoegd.\MessageBreak}%
}
\def\commissie#1{\global\def\aes@commissie{#1}}
\def\aes@activiteit{%
  \ClassWarning{boekje}{Geen \protect\activiteit\space gevonden.\MessageBreak
  Gebruik \protect\activiteit{TeX-cursus}\space om het in te stellen \MessageBreak
  Standaard (lege) activiteit ingevoegd.\MessageBreak}%
}
\def\activiteit#1{\global\def\aes@activiteit{#1}}
\def\aes@voorkant{
  \ClassError{boekje}{Geen \protect\voorkant\space gevonden.}{Definieer een voorkant, anders wordt het erg lelijk.
\MessageBreak}}
\def\voorkant#1{
   \aes@voorkant{#1}
   \begin{titlepage}
     \wpYoffset=\voffset
     \ThisCenterWallPaper{1}{kaft.jpg}
     \thispagestyle{empty}
     \mbox{} % vuil hekje om de pagina verder leeg te krijgen
   \end{titlepage}
   \thispagestyle{empty}
}

%achtergrond
\CenterWallPaper{\aes@achtergrond}

%headings
\pagestyle{fancy}
\fancyhf{}
\fancyhead[RE]{\aes@commissie}
\fancyhead[LO]{\aes@activiteit}
\fancyfoot[C]{\thepage}
\ifnumbering@art{
  \renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
}
\else{
  \renewcommand{\chaptermark}[1]{\markboth{#1}{}}
}\fi


%headings boven de chapter openingen.
\fancypagestyle{plain}{%
 \fancyhf{}
% \fancyhead[LE,RO]{\slshape \leftmark}
 %\fancyfoot[LE,RO]{\thepage}
 \fancyfoot[C]{\thepage}
 \ifnumbering@art{
   \renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ ##1}{}}
 }
 \else{
   \renewcommand{\chaptermark}[1]{\markboth{##1}{}}
 }\fi
}

% wat custom dingen
\newcommand{\url}[1]{\mbox{\texttt{#1}}}
\newcommand{\auteur}[1]{\begin{center} {\large #1} \end{center}}
\let\kop\subsection
\endinput

% vim: syntax=tex
