% Copyright (C) 2009-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: contract.cls 758 2015-05-13 20:58:21Z aldo $

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{contract}[2012/12/06 v1.2 A-Eskwadraat class voor contracten]
\LoadClassWithOptions{article}

\RequirePackage{aes}
% \RequirePackage[dutch]{babel}
\RequirePackage{a4wide}
\RequirePackage{fancyhdr}
\RequirePackage{xspace}
\RequirePackage{ifthen}
%\RequirePackage{atbeginend}
\RequirePackage{substr}
\RequirePackage{xkvview}
\RequirePackage{aestaal}
\RequirePackage{calc}

\newboolean{engels}
\setboolean{engels}{false}
%  \newcommand{\maakengels}{\setboolean{engels}{true}}

% BTW-shizzle
\newboolean{aes@optbtw}
\newcommand\welbtw{\setboolean{aes@optbtw}{true}}
\newcommand\geenbtw{\setboolean{aes@optbtw}{false}}

% Class option "noparaafbox". Default is: wel paraafbox tonen
\newcommand{\showparaafbox}{1}

\DeclareOption{english}{\setboolean{engels}{true}}
\ProcessOptions

 \ifthenelse{\boolean{engels}}{%
 \RequirePackage[english]{babel}}{\RequirePackage[dutch]{babel}}
%\DeclareOption{geenparaafbox}{\renewcommand{\showparaafbox}{0}}
%\ProcessOptions

%Onderstaande werkt niet, we weten niet hoe we het kunnen maken
% Ruimte besparen bij itemize en enumerate
%\AfterBegin{itemize}{\setlength{\itemsep}{-\parsep}}
%\AfterBegin{enumerate}{\setlength{\itemsep}{-\parsep}}

% Font
\usepackage[T1]{fontenc}
\usepackage{avant}
\renewcommand*\familydefault{\sfdefault}

\setlength{\parindent}{0cm}
\setlength{\textheight}{20.5cm}
\setlength{\footskip}{2.2cm}

% Pagestyle
\pagestyle{fancy}
\lhead{}
\rhead{}
\chead{\Large\textbf{\TAALovereenkomst}}

\newcommand{\islastpage}{\equal{\thepage}{\pageref{lastpage}}}
\newcommand{\thepagetext}{pagina \thepage\ van \pageref{lastpage}}
\ifthenelse{\equal{\showparaafbox}{1}}{% Paraafbox op elke pagina tonen
	% Laatste pagina? Dan paginanummer in cfoot
	% Niet laatste pagina? Dan grote streep in cfoot
	\cfoot{\ifthenelse{\islastpage}{\TAALpaginavan{\thepage}{\pageref{lastpage}}}{\vspace*{-2cm}\hrulefill}}

	% Laatste pagina? Dan niets in lfoot
	% Niet laatste pagina? Dan paginanummering in lfoot
	\lfoot{\ifthenelse{\islastpage}{}{\hspace*{2.5cm}\TAALpaginavan{\thepage}{\pageref{lastpage}}}}

	% Laatste pagina? Dan niets in rfoot
	% Niet laatste pagian? Dan paraafbox in rfoot
	\rfoot{\ifthenelse{\islastpage}{}{\parbox{6.7cm}{\paraafbox}}}
}{ % Geen paraafbox op elke pagina tonen: gewoon paginanummer in cfoot
	\lfoot{}%
	\cfoot{\TAALpaginavan{\thepage}{\pageref{lastpage}}}%
	\rfoot{}%
}

\newcommand{\toelichting}[1]{
	\emph{\small \TAALtoelichting #1}
}

\renewcommand{\aes}{\mbox{\emph{A--Eskwadraat}}\xspace}

%% Commando's voor de tegenpartij
\newcommand{\@tegenpartijkort}{
	\ClassError{contract}{Je bent vergeten de verkorte naam van\MessageBreak
                        de tegenpartij op te geven in je contract.\MessageBreak
                        Gebruik het commando `\protect\settegenpartijkort{Piet}'}{}
}

\newcommand{\settegenpartijkort}[1]{\renewcommand{\@tegenpartijkort}{#1}}
\newcommand{\tegenpartijkort}{\emph{\@tegenpartijkort}\xspace}

\newcommand{\@Tegenpartijkort}{\@tegenpartijkort}
\newcommand{\setTegenpartijkort}[1]{\renewcommand{\@Tegenpartijkort}{#1}}
\newcommand{\Tegenpartijkort}{\emph{\@Tegenpartijkort}\xspace}

\newcommand{\@tegenpartij}{NDEF}
\newcommand{\@tegenpartijdefined}{false}
\newcommand{\settegenpartij}[1]{\renewcommand{\@tegenpartij}{#1}}
\newcommand{\tegenpartij}{\@tegenpartij}%

\newcommand{\spons}{\tegenpartijkort}
\newcommand{\Spons}{\Tegenpartijkort}

%% Commando's voor de primaire partij (standaard A-Eskwadraat)
\newboolean{is@aeskwadraat}
\setboolean{is@aeskwadraat}{true}
\newcommand{\geen@aeskwadraat}{%
  \ifthenelse{\boolean{is@aeskwadraat}}{%
    % De partij was A-Eskwadraat, maar wordt nu veranderd, dus alle standaard-
    % waardes (die naar A-Eskwadraat verwijzen) leeggooien.
    \renewcommand{\@partijkort}{???}%
    \renewcommand{\@Partijkort}{???}%
    \renewcommand{\@partij}{???}%
    \setpartijopmerking{}%
		\setondertekeningen 1 1%
  }{}%
  \setboolean{is@aeskwadraat}{false}%
}

\newcommand{\@partijkort}{\aesnaam{}}
\newcommand{\setpartijkort}[1]{\geen@aeskwadraat\renewcommand{\@partijkort}{#1}}
\newcommand{\partijkort}{\emph{\@partijkort}\xspace}

\newcommand{\@Partijkort}{\@partijkort}
\newcommand{\setPartijkort}[1]{\geen@aeskwadraat\renewcommand{\@Partijkort}{#1}}
\newcommand{\Partijkort}{\emph{\@Partijkort}\xspace}

\newcommand{\@partij}{%
	\TAALaesnaam\\
	Princetonplein 5\\
	3584~CC~~Utrecht\TAALaesland
}
\newcommand{\setpartij}[1]{\geen@aeskwadraat\renewcommand{\@partij}{#1}}
\newcommand{\partij}{\@partij}%

\newcommand{\@partijopmerking}{Bestuursleden zijn gezamenlijk tekeningsbevoegd}
\newcommand{\setpartijopmerking}[1]{\renewcommand{\@partijopmerking}{#1}}

% Check dat de primaire partij A-Eskwadraat is en gooi anders een error.
\newcommand\AlleenVoorAEskwadraat[1]{%
  \ifthenelse{\boolean{is@aeskwadraat}}{}{%
    \ClassError{contract}{\protect#1\space is alleen beschikbaar wanneer\MessageBreak
                          A-Eskwadraat de primaire partij is}{%
                          Geef geen \protect\setpartij,
                          \protect\setpartijkort\space of \protect\setPartijkort\space op.}%
  }%
}

%% Commando om aantal ondertekeningen in te stellen

\newcounter{onder@tegenpartij}
\newcounter{onder@partij}
\newcommand{\setondertekeningen}[2]{%
	\setcounter{onder@tegenpartij}{#1}%
	\setcounter{onder@partij}{#2}%
}
\setondertekeningen 1 2


\newcommand{\partijen}{
	\section*{\TAALpartijen}
	\begin{tabular*}{\textwidth}{ll}
	\begin{minipage}{0.5\textwidth}
	\tegenpartij~\\~\\
	\noindent\textbf{\TAALhiernatenoemen\ ``\tegenpartijkort''}
	\end{minipage} & 

	\begin{minipage}{0.5\textwidth}
  \partij~\\~\\
	\noindent\textbf{\TAALhiernatenoemen\ ``\partijkort''}
	\end{minipage}

	\end{tabular*}~\\~\\

	\TAALakkoord{\Tegenpartijkort}{\partijkort}{\pageref{lastpage}} \ifaes@optbtw \TAALexclbtw \fi
}

% Annuleringsvoorwaarden
\newboolean{met@annuleringsvoorwaarden}
\setboolean{met@annuleringsvoorwaarden}{false}

\newcommand{\annulering}{

  \TAALannulering
  \setboolean{met@annuleringsvoorwaarden}{true}
}

% Verkorte notatie voor \ifthenelse: als parameter 1 en 2 equal zijn,
% dan wordt #3 uitgevoerd, anders #4
\newcommand{\@ifequal}[4]{\ifthenelse{\equal{#1}{#2}}{#3}{#4}}

% Maakt van een getal als "1" \'e\'en, "2" wordt "twee", etc. Werkt niet voor getallen groter dan "30", behalve de tientallen
%\newcommand{\@cijfertekst}[1]{%
	%\@ifequal{#1}{1}{\'e\'en}{}\@ifequal{#1}{2}{twee}{}\@ifequal{#1}{3}{drie}{}\@ifequal{#1}{4}{vier}{}\@ifequal{#1}{5}{vijf}{}%
	%\@ifequal{#1}{6}{zes}{}\@ifequal{#1}{7}{zeven}{}\@ifequal{#1}{8}{acht}{}\@ifequal{#1}{9}{negen}{}\@ifequal{#1}{10}{tien}{}%
	%\@ifequal{#1}{11}{elf}{}\@ifequal{#1}{12}{twaalf}{}\@ifequal{#1}{13}{dertien}{}\@ifequal{#1}{14}{veertien}{}\@ifequal{#1}{15}{vijftien}{}%
	%\@ifequal{#1}{16}{zestien}{}\@ifequal{#1}{17}{zeventien}{}\@ifequal{#1}{18}{achttien}{}\@ifequal{#1}{19}{negentien}{}\@ifequal{#1}{20}{twintig}%{}%
	%\@ifequal{#1}{30}{dertig}{}\@ifequal{#1}{40}{veertig}{}\@ifequal{#1}{50}{vijftig}{}%
	%\@ifequal{#1}{60}{zestig}{}\@ifequal{#1}{70}{zeventig}{}\@ifequal{#1}{80}{tachtig}{}\@ifequal{#1}{90}{negentig}{}%
%}

% Herhaal #2 #1 keer, steeds gescheiden door #3.
\newcounter{tel@plekken}
\newcommand{\herhaal}[3]{%
	#2%
	\setcounter{tel@plekken}{1}%
	\whiledo{\thetel@plekken < #1}{%
		#3%
		#2%
		\stepcounter{tel@plekken}%
	}%
}

\newcommand{\ondertekening}{
	\section*{\TAALondert}
	\fbox{\begin{minipage}[t]{\textwidth}~\\~
	\TAALopgemaakt
  \ifthenelse{\boolean{is@aeskwadraat}}{Utrecht}{\ldots\ldots\ldots\ldots\ldots\ldots\ldots},
  d.d. \ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\\~\bigskip\bigskip

	\begin{tabular*}{\textwidth}{p{0.5\textwidth} p{0.45\textwidth}}
	\hspace*{-0.2cm}\begin{minipage}[t]{0.5\textwidth}
	\begin{center}\textbf{\Tegenpartijkort}\\~\end{center}
	\herhaal{\theonder@tegenpartij}{%
		\TAALnaam : \ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\\
		\TAALhandtekening :
		\vspace*{2.5cm}
	}{~\\\bigskip

	}
	\end{minipage} &
	
	\hspace*{-0.4cm}\begin{minipage}[t]{0.5\textwidth}
	\begin{center}\textbf{\Partijkort}\\\begin{footnotesize}%
    \@ifequal{\@partijopmerking}{}{~}{\emph{\mbox{(\@partijopmerking)}}}%
  \end{footnotesize}\end{center}
	\herhaal{\theonder@partij}{%
		\TAALnaam : \ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\ldots\\
		\TAALhandtekening :
		\vspace*{2.5cm}
	}{~\\\bigskip

	}
  \end{minipage}\vspace*{0.75cm}
	\end{tabular*}
	\label{lastpage}
	\end{minipage}}
}



% Genereert standaardtekst voor Vakidioot-adverentie n.a.v. parameters in keyval-style, bijv:
%   \vakid{collegejaar=2008-2009,editie=3,editiemaand=mei}
%
% Verplichte parameters:
%  collegejaar: spreekt voor zich. Bijv. '2008-2009'
%  editie: nummer Vakidioot, bijv '4' of '4 en 5'
%  editiemaand: maand waarin de Vakid zal verschijnen, bijvoorbeeld 'april' of 'april en juni'
%
% Optionele parameters:
%  aantal: aantal advertentie, bijv '1' of '2' (default: 1)
%  plaatsing: plaats van advertentie: binnenwerk, binnen-voor, binnen-achter, achter (default: binnenwerk)
%  formaat: omvang advertentie: enkel, dubbel (default: enkel)
%  type: type advertentie: advertentie, advertorial (default: advertentie)
%
% Voorbeeldaanroepen:
%  - advertorial binnenkant achterkaft in Vakid 3 in mei van het collegejaar 2008-2009:
%    \vakid{collegejaar=2008-2009,editie=3,editiemaand=mei,plaatsing=binnen-achter,type=advertorial}
%  - twee advertenties in twee vakidioten:
%    \vakid{collegejaar=2008-2009,editie=1 en 3,editiemaand=oktober en februari}
\define@key{vakid}{collegejaar}{\def\vakidadvcollegejaar{#1}}
\define@key{vakid}{editie}{\def\vakidadveditie{#1}}
\define@key{vakid}{editiemaand}{\def\vakidadveditiemaand{#1}}
\define@key{vakid}{bedrag}{\def\vakidadvbedrag{#1}}
\define@choicekey{vakid}{plaatsing}{binnenwerk,binnen-voor,binnen-achter,achter}{\def\vakidadvplaatsing{#1}}
\define@choicekey{vakid}{formaat}{enkel,dubbel}{\def\vakidadvformaat{#1}}
\define@choicekey{vakid}{type}{advertentie,advertorial}{\def\vakidadvtype{#1}}
\define@choicekey{vakid}{nosection}{yes,no}[yes]{\def\vakidadvnosection{#1}}
\newcommand{\@vakidadvstrmaand}{UNDEF}
\newcommand{\@vakidadvstreditie}{UNDEF}
\newcommand{\@vakidadvstrzal}{UNDEF}
\newcommand{\vakid}[1]{
 \AlleenVoorAEskwadraat{\vakid}%
 \setkeys{vakid}{#1} % key-value pairs parsen

 % Verplichte parameters checken
 \@ifundefined{vakidadvcollegejaar}{\ClassError{contract}{Je bent vergeten het collegejaar te specificeren\MessageBreak bij de Vakid-advertentie!}~}{}
 \@ifundefined{vakidadveditie}{\ClassError{contract}{Je bent vergeten de Vakid-editie te specificeren\MessageBreak bij de Vakid-advertentie!}~}{}
 \@ifundefined{vakidadveditiemaand}{\ClassError{contract}{Je bent vergeten de maand van Vakid-editie \vakidadveditie\MessageBreak te specificeren bij de Vakid-advertentie!}~}{}

 % Bedrag? Default: niet tonen
 \@ifundefined{vakidadvbedrag}{\def\vakidadvbedrag{}}

 % Section{...} skippen? Default: niet skippen
 \@ifundefined{vakidadvnosection}{\def\vakidadvnosection{no}}

 % Plaats van advertentie bepalen
 \@ifundefined{vakidadvplaatsing}{\newcommand{\vakidadvplaatsing}{binnenwerk}}{}
 \ifthenelse{\equal{\vakidadvplaatsing}{binnenwerk}}{\renewcommand{\vakidadvplaatsing}{in het binnenwerk}}{}
 \ifthenelse{\equal{\vakidadvplaatsing}{achter}}{\renewcommand{\vakidadvplaatsing}{op de achterzijde}}{}
 \ifthenelse{\equal{\vakidadvplaatsing}{binnen-voor}}{\renewcommand{\vakidadvplaatsing}{aan de binnenzijde van de voorkaft}}{}
 \ifthenelse{\equal{\vakidadvplaatsing}{binnen-achter}}{\renewcommand{\vakidadvplaatsing}{aan de binnenzijde van de achterkaft}}{}
 
 % Formaat advertentie (enkel, dubbel) vaststellen
 \@ifundefined{vakidadvformaat}{\newcommand{\vakidadvformaat}{enkel}}{}
 \ifthenelse{\equal{\vakidadvformaat}{enkel}}{\renewcommand{\vakidadvformaat}{}}{}
 \ifthenelse{\equal{\vakidadvformaat}{dubbel}}{\renewcommand{\vakidadvformaat}{dubbele\xspace}}{}
 
 % Type advertentie (advertentie, advertorial) vaststellen
 \@ifundefined{vakidadvtype}{\newcommand{\vakidadvtype}{advertentie}}{}
 \ifthenelse{\equal{\vakidadvtype}{advertentie}}{\renewcommand{\vakidadvtype}{advertentie}}{}
 \ifthenelse{\equal{\vakidadvtype}{advertorial}}{\renewcommand{\vakidadvtype}{advertorial}}{}
 
 % Meerdere edities?
 \renewcommand{\@vakidadvstreditie}{editie}
 \renewcommand{\@vakidadvstrzal}{zal}
 \IfSubStringInString{ en }{\vakidadveditie}{%
	\IfSubStringInString{ en }{\vakidadveditiemaand}{}{\ClassError{contract}{Meerdere edities gespecificeerd voor\MessageBreak Vakid-advertentie, maar slechts 1 maand?!}~}
 	\renewcommand{\@vakidadvstreditie}{edities}{}
 	\renewcommand{\@vakidadvstrzal}{zullen}{}
 }{}

 % Meerdere maanden?
 \renewcommand{\@vakidadvstrmaand}{maand}
 \IfSubStringInString{ en }{\vakidadveditiemaand}{%
 	\IfSubStringInString{ en }{\vakidadveditie}{}{\ClassError{contract}{Meerdere maanden gespecificeerd voor\MessageBreak Vakid-advertentie, maar slechts 1 editie?!}~}

 	\renewcommand{\@vakidadvstrmaand}{maanden}
 }{}
 
 \ifthenelse{\equal{\vakidadvnosection}{yes}}{}{\section{\TAALvakidad}}
 \TAALvakidadtext{\tegenpartijkort}{\vakidadvcollegejaar}{\vakidadvformaat}{\vakidadvtype\ \vakidadvplaatsing}{\@vakidadvstreditie\ \vakidadveditie}{\@vakidadvstreditie\ \@vakidadvstrzal}{\@vakidadvstrmaand\ \vakidadveditiemaand}
  \ifthenelse{\equal{\vakidadvbedrag}{}}{}{\bedrag{\vakidadvbedrag}}

 % Zorgen dat values uit deze vakid-advertentie niet per abuis in volgende advertenties terecht komen
 \let\vakidadvcollegejaar\@undefined
 \let\vakidadvformaat\@undefined
 \let\vakidadvtype\@undefined
 \let\vakidadvplaatsing\@undefined
 \let\vakidadveditie\@undefined
 \let\vakidadveditiemaand\@undefined
 \let\vakidadvnosection\@undefined
}

% Commando om standaardtekst voor vacaturebank te tonen
\define@key{vacaturebank}{periode}{\def\vbankperiode{#1}}
\define@key{vacaturebank}{aantal}{\def\vbankaantal{#1}}
\define@key{vacaturebank}{bedrag}{\def\vbankbedrag{#1}}
\define@choicekey{vacaturebank}{nosection}{yes,no}[yes]{\def\vbanknosection{#1}}
\newcommand{\@vbanktxtvacature}{UNDEF}
\newcommand{\vacaturebank}[1]{
 \AlleenVoorAEskwadraat{\vacaturebank}%
 \setkeys{vacaturebank}{#1} % key-value pairs parsen

 % Verplichte parameters checken
 \@ifundefined{vbankperiode}{\ClassError{contract}{Je bent vergeten de periode te specificeren,\MessageBreak gedurende welke de vacaturebank gebruikt mag worden!}~}{}
 \@ifundefined{vbankaantal}{\def\vbankaantal{1}}{}
 \@ifundefined{vbankbedrag}{\def\vbankbedrag{}}{}

 % \section{...} skippen? Default: niet skippen
 \@ifundefined{vbanknosection}{\def\vbanknosection{no}}

 % is het 'vacature' of 'vacatures'?
 \renewcommand{\@vbanktxtvacature}{\ifthenelse{\equal{\vbankaantal}{1}}{\TAALvacature}{\TAALvacatures}\xspace}
  \newcommand{\@vbanktxtthisthese}{\ifthenelse{\equal{\vbankaantal}{1}}{\TAALthis}{\TAALthese}\xspace}

 \ifthenelse{\equal{\vbanknosection}{yes}}{}{\section{\TAALvacbank}}
 \TAALvacbanktxt{\tegenpartijkort}{\vbankperiode}{\@cijfertekst{\vbankaantal}}{\@vbanktxtvacature}{\Tegenpartijkort}{\@vbanktxtthisthese}{\@vbanktxtvacature}%
%
\ifthenelse{\equal{\vbankbedrag}{}}{}{\bedrag{\vbankbedrag}}
%

 \let\vbankperiode\@undefined
 \let\vbankaantal\@undefined
 \let\vbankbedrag\@undefined
}

% Commando om standaardtekst voor mailing te tonen
\define@key{stagemailing}{maand}{\def\stagemailmaand{#1}}
\define@key{stagemailing}{bedrag}{\def\stagemailbedrag{#1}}
\define@choicekey{stagemailing}{nosection}{yes,no}[yes]{\def\stagemailnosection{#1}}
\newcommand{\stagemailing}[1]{%
 \AlleenVoorAEskwadraat{\stagemailing}%
 \setkeys{stagemailing}{#1} % key-value pairs parsen
 %
 % Verplichte parameters checken
 \@ifundefined{stagemailmaand}{\ClassError{contract}{Je bent vergeten de maand te specificeren\MessageBreak waarin de stagemail verstuurd wordt!}~}{}
 %
 % \section{...} skippen? Default: niet skippen
 \@ifundefined{stagemailnosection}{\def\stagemailnosection{no}}{}
 %
 \ifthenelse{\equal{\stagemailnosection}{yes}}{}{\section{\TAALstagemail}}
 \TAALstagemailtxt{\spons}{\stagemailmaand}
 
 %
 \ifthenelse{\equal{\stagemailbedrag}{}}{}{\bedrag{\stagemailbedrag}}
 %
 \let\stagemailbedrag\@undefined
 \let\stagemailmaand\@undefined
 \let\stagemailnosection\@undefined
}

% Print een bedrag en zorgt voor een optelling van alle bedragen
\newcounter{@totaalbedrag}
\newcommand{\bedrag}[1]{ \TAALbedragsteun{\Tegenpartijkort}{\partijkort}{#1}
  \addtocounter{@totaalbedrag}{#1}}
\newcommand{\minbedrag}[1]{ \TAALbedragrestitutie{\Tegenpartijkort}{\partijkort}{#1}
  \addtocounter{@totaalbedrag}{#1} }

% Stelt de betalingstermijn in (in dagen)
\newcommand{\@betalingstermijn}{28}
\newcommand{\betalingstermijn}[1]{\renewcommand{\@betalingstermijn}{#1}}

% Stelt percentage korting in die over het totaalbedrag gegeven wordt
\newcounter{@kortingspercentage}\setcounter{@kortingspercentage}{0}
\newcommand{\kortingspercentage}[1]{\setcounter{@kortingspercentage}{#1}}

% Stelt bedrag in dat als ruwe korting wordt gegeven
\newcounter{@kortingsbedrag}\setcounter{@kortingsbedrag}{0}
\newcommand{\kortingsbedrag}[1]{\setcounter{@kortingsbedrag}{#1}}

% Overschrijft de standaard betalingsvoorwaarden met een eigen tekst
\newcommand{\betalingsvoorwaarden}[1]{\renewcommand{\TAALbetalingsvoorwaardentext}[3]{#1}}

\newcommand{\@testcommand}[1]{#1}

% Vertaalt cijfers in tekst tot maximaal 9999
\newcommand{\@zegge}[1]{%
	\@ifundefined{c@getal}{\newcount\getal}{}\getal=#1%
	\@ifundefined{c@rest}{\newcount\rest}{}\rest=\getal%
	\@ifundefined{c@klad}{\newcount\klad}{}%
	%
	% Duizendtal parsen (1800 wordt 1, 2001 wordt 2, etc)
	%\@ifundefined{c@\duizendtal}{\newcount\duizendtal}%\duizendtal=\getal%
	\@ifundefined{c@duizendtal}{\newcount\duizendtal}{}\duizendtal=\getal%
	\divide\duizendtal by 1000%
	\klad=\duizendtal%
	\multiply\klad by 1000 % rounding error voorkomen
	\advance\rest by -\klad%
	%
	% Honderdtal parsen (800 wordt 8, 190 wordt 1, etc)
	\@ifundefined{c@honderdtal}{\newcount\honderdtal}{}\honderdtal=\rest%
	\divide\honderdtal by 100%
	\klad=\honderdtal%
	\multiply\klad by 100%
	\advance\rest by -\klad%
	%
	% Tiental parsen (11 wordt 1, 28 wordt 2)
	\@ifundefined{c@tiental}{\newcount\tiental}{}\tiental=\rest%
	\divide\tiental by 10%
	\klad=\tiental%
	\multiply\klad by 10%
	\advance\rest by -\klad%
	%
	% Teksten uitschrijven
	\@ifundefined{duizendtaltekst}{\newcommand{\duizendtaltekst}{}}{\renewcommand{\duizendtaltekst}{}}%
	\@ifundefined{honderdtaltekst}{\newcommand{\honderdtaltekst}{}}{\renewcommand{\honderdtaltekst}{}}%
	\@ifundefined{tientalresttekst}{\newcommand{\tientalresttekst}{}}{\renewcommand{\tientalresttekst}{}}%
	%
	% Tekst duizendtal
	\@ifequal{\the\duizendtal}{0}{}{%
		\@ifequal{\the\duizendtal}{1}{% iets met 1000: uitschrijven ("zeventienhonderd")
			\@ifequal{\the\honderdtal}{0}{%
				\renewcommand{\duizendtaltekst}{\TAALduizend\ }%
			}{%
				\renewcommand{\honderdtaltekst}{duizendtal}%
			}%
		}{% > 2000: duizenden uitschrijven: tweeduizend, drieduizend, etc
			\renewcommand{\duizendtaltekst}{\@cijfertekst{\the\duizendtal}\TAALgetalspatie\TAALduizend\ }%
		}%
	}%
	%
	% Tekst honderdtal
	\ifthenelse{\equal{\honderdtaltekst}{duizendtal}}{% Honderdtal en duizendtal samenvoegen ('zeventienhonderd')
		\klad=\duizendtal%
		\multiply\klad by 10%
		\advance\klad by \honderdtal%
		\renewcommand{\duizendtaltekst}{}%
		\renewcommand{\honderdtaltekst}{\@cijfertekst{\the\klad}\TAALhonderd\ }%
	}{% Honderdtal staat op zichzelf: vijfhonderd, zeshonderd
		\@ifequal{\the\honderdtal}{0}{}{%
			\renewcommand{\honderdtaltekst}{\@cijfertekst{\the\honderdtal}\TAALgetalspatie\TAALhonderd\ }%
		}%
	}%
	%
	% Tekst tiental + rest
	% Pas op voor special cases als (twee\"entwintig, drie\"envijftig)
	\@ifundefined{c@kladd}{\newcount\kladd}{}%
	\@tientaltekst{\tiental}{\tientalresttekst}%
	%
	\duizendtaltekst \honderdtaltekst \tientalresttekst%
}

\newcommand\rondaf[1]{% Input van het bedrag moet x1000, dus 6,43 wordt 6430
	\newcounter{bedrag}%
	\setcounter{bedrag}{#1}%
	%
	\newcounter{euro}%
	\newcounter{cent}%
	\newcounter{rest}%
	%
	\newcount \euros%
	\newcount \centen%
	%
	% Bereken het aantal hele euro's
	\euros = \thebedrag%
	\divide \euros by 1000%
	\setcounter{euro}{\the\euros}%
	%
	% Bereken het aantal centen met 1 decimaal
	\multiply \euros by 1000%
	\setcounter{cent}{\thebedrag-\the\euros}%
	%
        % Bereken het aantal hele centen
	\centen = \thecent%
	\divide \centen by 10%
	\multiply \centen by 10%
	%
	% Sla de eerste decimaal van het aantal centen op
	\setcounter{rest}{\thecent-\the\centen}%
	%
	% Sla het aantal centen op
	\divide \centen by 10%
	\setcounter{cent}{\centen}%
	%
	% Als het laatste getal groter of gelijk aan 5 is, hoog de centen met 1 op
	\ifthenelse{\therest < 5}{}{\addtocounter{cent}{1}}%
	%
	% Als door de laatste actie het aantal centen 100 is geworden, hoog de euro's met eentje op
	\ifthenelse{\thecent < 100}{}{%
		\addtocounter{euro}{1}%
		\setcounter{cent}{0}%
	}%
	%
	% Check of er centen nodig zijn
	\ifthenelse{\thecent > 0}{%
		\theeuro,\ifthenelse{\thecent<10}{0}{}\thecent\ euro (\TAALzegge: \@zegge{\theeuro} \TAALeuros\ \TAALen\ \@zegge{\thecent} \TAALcents)%
	}{%
		\theeuro\ euro (\TAALzegge: \@zegge{\theeuro} \TAALeuros)%
	}%
}

\newcommand{\smallspace}{\hspace*{0.05cm}}

% Print informatie over betaling (totaalbedrag, betalingstermijn, moment van factureren, etc)
\newcommand{\totaalenvoorwaarden}[1]{%
 \section{\TAALbetalingsvoorwaarden}%
 %
 \newcount \totaalkorting % wordt totaal kortingsbedrag (na verwerken @kortingspercentage en @kortingsbedrag)
 \newcount \totaalnakorting % wordt nieuw totaalbedrag na verrekening \totaalkorting
 \newcount \totaalkortingspercentage % wordt het totale kortingspercentage (@kortingspercentage + @kortingsbedrag) in berekend
 \newcount \totaalnakortingmetbtw % het uiteindelijke totaalbedrag na berekenen btw
 %
 % Start verwerken @kortingspercentage
 \totaalkorting=\the@totaalbedrag%
 \multiply \totaalkorting by \the@kortingspercentage%
 \divide \totaalkorting by 100%
 \totaalnakorting=\the@totaalbedrag%
 %
 % Extra bonus: @kortingsbedrag
 \advance \totaalkorting by \the@kortingsbedrag%
 \advance \totaalnakorting by -\totaalkorting%
 %
 % Berekenen totale kortingspercentage
 \ifthenelse{\equal{\the\totaalkorting}{0}}{\totaalkortingspercentage=0}{%
	 \totaalkortingspercentage=\totaalkorting%
	 \multiply \totaalkortingspercentage by 100%
	 \divide \totaalkortingspercentage by \the@totaalbedrag%
 }%
 %
 % Bereken de btw, indien nodig
 \ifaes@optbtw%
   \totaalnakortingmetbtw = \totaalnakorting%
   \multiply \totaalnakortingmetbtw by 1210%
 \fi%
 %
 \ifthenelse{\the\totaalkorting=0}{% Geen korting
  \ifaes@optbtw%
    \TAALtotaalgeenkortingbtw{\the@totaalbedrag}{\rondaf{\the\totaalnakortingmetbtw}}%
  \else%
    \TAALtotaalgeenkorting{\the@totaalbedrag}%
  \fi%
 }{ % Wel korting
  \ifaes@optbtw%
    \TAALtotaalwelkortingbtw{\the@totaalbedrag}{\Partijkort}{\tegenpartijkort}{\the\totaalkorting}{\the\totaalkortingspercentage}{\the\totaalnakorting}{\rondaf{\the\totaalnakortingmetbtw}}%
  \else%
    \TAALtotaalwelkorting{\the@totaalbedrag}{\Partijkort}{\tegenpartijkort}{\the\totaalkorting}{\the\totaalkortingspercentage}{\the\totaalnakorting}%
  %De onderdelen van deze overeenkomst vormen samen een bedrag van \ euro.  biedt  een korting van \ euro (\%), waarmee het totaalbedrag van deze overeenkomst uitkomt op \ euro (zegge:\@zegge{\the\totaalnakorting} euro).
  \fi%
 }%
~\\

 \TAALbetalingsvoorwaardentext{\tegenpartijkort}{\@betalingstermijn}{\partijkort}
 
 \ifthenelse{\boolean{met@annuleringsvoorwaarden}}{%
 \section{\TAALannuleringsvoorwaarden}
\TAALannuleringsvoorwaardentext{\tegenpartijkort}{\partijkort}
 }{}
}

\newcommand{\paraafbox}{
 \begin{footnotesize}
 \fbox{\begin{tabular*}{6.5cm}{l|l}
 \multicolumn{2}{c}{\TAALparaaf}\\
 \parbox{3cm}{\centering\tegenpartijkort} & \parbox{3cm}{\centering\partijkort}\\
 ~ & ~\\
 ~ & ~\\
 ~ & ~\\
 ~ & ~
 \end{tabular*}}
 \end{footnotesize}
}

\AtBeginDocument{\partijen~\\}

\AtEndDocument{%
 % zet alleen betalingsvoorwaarden neer als er überhaupt iets te betalen is
 \ifthenelse{\equal{\the@totaalbedrag}{0}}{~}{\totaalenvoorwaarden\\~}%
 \ondertekening%
}

% vim: nospell syntax=tex
