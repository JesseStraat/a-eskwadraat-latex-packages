% Copyright (C) 2001-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: aanmeldingsform.cls 807 2017-04-04 10:53:24Z aldo $

%Uitleg werking en nut: Deze class zorgt voor de lay-out van het aanmeldingsformulier dat de eerstejaars (of masters) tijdens de intro ondertekenen. Het daadwerkelijke document dat hiervoor gebruikt wordt staat op het account van het bestuur, waar tevens een versie van de class staat. De class valt dus niet direct meer onder ons versiebeheer

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{aanmeldingsform}[2007/09/13 v0.2 Het A-Eskwadraat aanmeldingsformulier]

% Options ingevoerd voor mooiere code voor masterlidmaatschap, tevens gebruikt voor de Engelse versie inschrijfform. Uiteindelijk zijn twee opties nodig om een goed formulier te krijgen, te weten taal en soort lid. 

\DeclareOption{nederlands}{\PassOptionsToPackage{dutch}{babel}
			\PassOptionsToPackage{nederlands}{optional}} % moet nederlands zijn, dutch werkt niet.
\DeclareOption{english}{\PassOptionsToPackage{dutch,english}{babel}
			\PassOptionsToPackage{english}{optional}}
\DeclareOption{lid}{\PassOptionsToPackage{lid}{optional}}
\DeclareOption{masterlid}{\PassOptionsToPackage{masterlid}{optional}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ProcessOptions

\RequirePackage[none]{optional}  % optional vereist tenminsten \'e\'en optie
\RequirePackage{ifthen}
\RequirePackage{babel}
\RequirePackage{database}
\RequirePackage{tabularx}
\RequirePackage{multicol}
\RequirePackage{graphicx}
\RequirePackage{pifont}
\RequirePackage{eurosym}
\RequirePackage{xspace}
\RequirePackage[utf8]{inputenc}
\RequirePackage{aes}

\LoadClass[12pt]{article}

% vertical dimensions
\voffset=-1in
\topmargin=0mm
\headheight=0pt
\headsep=0pt
\textheight=297mm
\footskip=0pt
\parskip0pt

% horizontal dimensions
\hoffset=-1in
\oddsidemargin=15mm
\evensidemargin=15mm
\textwidth=180mm
\marginparsep=0pt
\marginparwidth=0pt
\parindent=0pt

\pagestyle{empty}

\font\tekstfont=ptmr scaled 1300
\font\omschfont=ptmr scaled 1300
\font\titelfont=ptmr scaled 3000

\def\hokje{\ding{111}}


% Definities voor het in te vullen blok in twee talen

\opt{nederlands}{
\def\Lidnummer{Lidnummer}
\def\Mentorgroep{Mentorgroep}
\def\Voornaam{Voornaam}
\def\Voorletters{Voorletters}
\def\Tussenvoegsels{Tussenvoegsels}
\def\Achternaam{Achternaam}
\def\Geslacht{Geslacht}
\def\Geboortedatum{Geboortedatum}
\def\Straat{Straat}
\def\Huisnummer{Huisnummer}
\def\Postcode{Postcode}
\def\Woonplaats{Woonplaats}
\def\Telefoon{Telefoon}
\def\Mobielnummer{Mobiel nummer}
\def\Telefoonouders{Telefoon ouders}
\def\Email{Email}
\def\Studentnummer{Studentnummer}
\def\Studie{\opt{lid}{Studie}\opt{masterlid}{Masterprogramma}}
}

\opt{english}{
\def\Lidnummer{Membershipnumber}
\def\Mentorgroep{Mentorgroup}
\def\Voornaam{First name}
\def\Voorletters{Initials}
\def\Tussenvoegsels{Tussenvoegsels}
\def\Achternaam{Surname}
\def\Geslacht{Gender}
\def\Geboortedatum{Date of birth}
\def\Straat{Street}
\def\Huisnummer{Number}
\def\Postcode{Postal code}
\def\Woonplaats{Residence}
\def\Telefoon{Telephone number}
\def\Mobielnummer{Cell phone}
\def\Telefoonouders{Phonenumber parents}
\def\Email{Email}
\def\Studentnummer{Studentnumber}
\def\Studie{\opt{lid}{Study}\opt{masterlid}{Programme}}
}


% Volgende vier commando's zijn nodig voor tekst achter de hokjes
%\newcommand{\opvraagbaar}{
%\opt{nederlands}{Ik wil niet dat mijn adresgegevens via \textsc{www} opvraagbaar zijn voor \hokje \ leden en \hokje \ oud-leden}
%\opt{english}{I don't want my address information to be viewable over \textsc{www} for other \hokje \ members and \hokje \ old-members}
%}
%\newcommand{\alma}{
%\opt{nederlands}{Ik wil niet dat mijn adresgegevens en foto in de almanak komen.}
%\opt{english}{I don't want my address information to be placed in the almanak}
%}

%\newcommand{\ofoto}{
%\opt{nederlands}{Ik geef \aesnaam\ geen toestemming mijn universitaire Osiris-foto te
%  gebruiken voor haar ledenadministratiesyteem.}
%\opt{english}{I don't give permission to \aesnaam\ to use my university Osiris-photograph for its membershipadministration}
%}	
%
%\newcommand{\actmail}{
%\opt{nederlands}{Ik wil niet per email op de hoogte worden gehouden van \aesnaam-activiteiten.}
%\opt{english}{I don't want to be informed about \aesnaam-activities by email.}
%} 		

% Hokjes
\newcommand{\adres}{
\opt{nederlands}{Mijn adresgegevens en foto (z.o.z.):}
\opt{english}{Address information and photograph (p.t.o):}
}
\newcommand{\mailings}{
\opt{nederlands}{Mailings (z.o.z. voor toelichting):}
\opt{english}{Mailings (p.t.o. for clarification):}
}
\newcommand{\leden}{
\opt{nederlands}{Nee: \textbf{niet} opzoekbaar voor leden}
\opt{english}{No: \textbf{not} viewable for members}
}
\newcommand{\oudleden}{
\opt{nederlands}{Nee: \textbf{niet} opzoekbaar voor oudleden}
\opt{english}{No: \textbf{not} viewable for oldmembers}
}
\newcommand{\almanak}{
\opt{nederlands}{Nee: \textbf{niet} geplaatst in de almanak}
\opt{english}{No: \textbf{not} printed in the almanac}
}
\newcommand{\actmailing}{
\opt{nederlands}{Nee: \textbf{geen} wekelijkse activiteitenmail}
\opt{english}{No: \textbf{no} weekly mail about activities}
}
\newcommand{\stagemail}{
\opt{nederlands}{Nee: \textbf{geen} bimaandelijkse stagemail}
\opt{english}{No: \textbf{no} bimonthly internshipmail}
}
\newcommand{\vakid}{
\opt{nederlands}{Nee: \textbf{niet} 6 maal per jaar de VakIdioot}
\opt{english}{No: \textbf{no} VakIdioot 6 times a year}
}

% Uitleg over hokjes voor achterop
\newcommand{\toelichting}{
	\opt{nederlands}{
		\begin{center}\Large{\textbf{Toelichting aanvinkhokjes}}\end{center}

		Standaard kunnen je adresgegevens en foto via de website worden opgezocht door mensen die in kunnen loggen, 			te weten andere leden en oud-leden. In geen geval kunnen derden je gegevens opzoeken.  
		Tevens worden ze gepubliceerd in de almanak (jaarboek), die je in het eerste jaar gratis ontvangt. \\[1pt]

		Wekelijks ontvang je van \aesnaam een e-mail met de komende
		activiteiten en ongeveer eens in de twee maanden een e-mail over stages.\\[1pt]

		De voorkeuren, die je hier aangeeft, kan je te allen tijde wijzigen via de website.
	}
	\opt{english}{
		\begin{center}\Large{\textbf{Clarification checkboxes}}\end{center}
		Your adress information and photograph is defaultly viewable on the website by people, who can log in, that 			is other members and old-members. Under no circumstances will this information be attainable by thirds.
		Adress information and photograph will be published in the almanac, which you'll recieve for free in the 			first year.\\[1pt]

		Weekly you'll recieve an email of the coming \aesnaam activities and about twice a month an email 			about interesting internships. Unfortunatly these emails are in Dutch.\\[1pt]
	
		The preferences, you're listing at the front, can be changed at any time on the website.
	}
}	
 


% Stagelijst voor masters

%\newcommand{\stagelijst}{%
%\opt{masterlid}{\\[2pt]\hline 
%	\opt{nederlands}{
%  		\hokje & Meld mij aan voor de stagelijst van A--Eskwadraat, waarover eens per maand interessante 				stageaanbiedingen van bedrijven worden verstuurd.
%			}
%	\opt{english}{
%		\hokje & Subscribe me to the internshiplist of A--Eskwadraat, through which I will be informed on %			interesting internships once a month by mail.
%			} 
%	}
%}	


% Hoogte van lidmaatschapsgeld

\newcommand{\bedrag}{\opt{masterlid}{\euro{\,10,--}}\opt{lid}{\euro{\,30,--}}}

% De bepalingen in twee talen

\newcommand{\bepalingen}{

\opt{nederlands}{
  Ondergetekende geeft zich op als lid van Studievereniging \aesnaam.\\
  Het lidmaatschap vangt aan na betaling van de contributie\opt{lid}{\xspace(bij eerstejaarsboekenpakket inbegrepen)}.\\
  De contributie bedraagt \bedrag \ in het eerste jaar en \euro{\,0,00}
  in alle volgende studiejaren.\\
  Het lidmaatschap eindigt bij uitschrijving bij het departement, bij
  schriftelijke opzegging of bij overlijden.\\
  De hier verstrekte persoonsgegevens zullen vertrouwelijk worden behandeld en worden niet zonder toestemming aan derden   	verstrekt.
}

\opt{english}{
The undersigned subscribes as member of Study Association \aesnaam.\\
Membership wil commence upon payment of contribution fee, set at \bedrag \ in the first year and \euro{\,0,00} in all following years.  \\
Membership ends upon disenrollment at the department, a written termination or death. \\
The above address entries will be used in a confidential manner and will not be passed to a third party without permission}
}


% Definities voor het ondertekengedeelte

\opt{nederlands}{
	\def\Plaats{Plaats}
	\def\Handtekening{Handtekening}
	\def\Datum{Datum}
}
\opt{english}{
	\def\Plaats{Town/City}
	\def\Handtekening{Signature}
	\def\Datum{Date}
}


% Nog wat definities

\fboxsep=0pt

\def\oms#1{\mbox{\omschfont #1}}
\def\dat#1{\mbox{\vtop to12pt{\hsize=4cm \tt #1\vss}}}
\def\legeregel#1{&&&\\[#1]}
\def\afst{8pt}


\def\datumpje{} % met \today huidige datum automatisch invullen, meestal niet
		 	% handig
\def\datum#1{\def\datumpje{#1}}
\def\plaatsje{}
\def\plaats#1{\def\plaatsje{#1}}

% Hier begint het daadwerkelijke formulier
\def\formulier{%
\edef\leeg{}
\edef\utrecht{Utrecht}
\ifx\leeg\geslacht
	\edef\geslacht{\opt{nederlands}{M/V}\opt{english}{M/F}}
\fi


\null\vskip5mm
\centerline{\includegraphics[width=4cm]{a-es}}
\begin{center}
 {\titelfont
  \opt{nederlands}{Aanmeldingsformulier \\[\afst]
  		Studievereniging \aesnaam}
  \opt{english}{Subscriptionform \\[\afst]
  		Study Association \aesnaam}
 }
 \vskip12pt
  %
  \begin{tabularx}{\textwidth}{|lXlX|}
  \hline
  \legeregel{1pt}
  \oms{\Lidnummer}  & \dat{\lidnr} & \opt{lid}{\oms{\Mentorgroep}} & \opt{lid}{\dat{\mentorgroepnr}~}2007\\[\afst]
  \hline
  \hline
  \legeregel{1pt}
  \oms{\Voornaam}     & \dat{\voornaam}  &   \oms{\Voorletters} & \dat{\voorletters} \\[\afst]
  \oms{\Tussenvoegsels} & \dat{\tussenvoegsels} & \oms{\Achternaam}  & \dat{\achternaam} \\[\afst]
  \oms{\Geslacht}     & \dat{\geslacht} & \oms{\Geboortedatum}&   \dat{\geboortedatum}    \\[\afst]
  \hline
  \legeregel{1pt}
  \oms{\Straat}       & \dat{\straat} & \oms{\Huisnummer} & \dat{\huisnummer}\\[\afst]
  \oms{\Postcode}     & \dat{\postcode} & \oms{\Woonplaats}  & \dat{\woonplaats} \\[\afst]
  \oms{\Telefoon}    & \dat{\telnrA}    & \oms{\Mobielnummer}& \dat{\telnrB}\\[\afst]
  \oms{\Telefoonouders}   & \dat{\telnrC} & \oms{\Email}   & \dat{\email}\\[\afst]
  \hline
  \legeregel{1pt}
  \oms{\Studentnummer}  &  \dat{\studentnr} & \oms{\Studie} & \dat{\studie} \\[\afst]
  \hline
  \end{tabularx}

\bigskip
  \begin{tabularx}{\textwidth}{|lX|lX|}
  \hline
\multicolumn{2}{|l|}{\adres} & \multicolumn{2}{|l|}{\mailings}\\[2pt]
\hokje & \leden & \hokje & \actmailing\\[2pt]
\hokje & \oudleden & \hokje & \stagemail\\[2pt]
\hokje & \almanak & &\\[2pt] %\hokje & \vakid\\[2pt]
\hline
\end{tabularx}


\bigskip
\begin{minipage}{\textwidth}
  \tekstfont\baselineskip=14pt
 \bepalingen
\end{minipage}
\bigskip

\begin{tabularx}{\textwidth}{|lXlXX|}
\hline
& & & & \\[2pt]
\oms{\Plaats} & \dat{\plaatsje} & \oms{\Handtekening} & & \\[\afst]
\oms{\Datum} & \dat{\datumpje} & & & \\[\afst]
\hline
\end{tabularx}
\bigskip

\begin{minipage}{\textwidth}
\begin{center}
  \tekstfont\baselineskip=14pt
  \small \opt{nederlands}{In te vullen door}\opt{english}{To be filled in by} \aesnaam
\end{center}
\end{minipage}

\begin{tabularx}{\textwidth}{|XcX|XcX|}
  \hline
  & \tiny Contributie betaald (stempel \& paraaf) & &
  & \tiny Gegevens verwerkt (datum \& paraaf) & \\[24pt]
  \hline
  \end{tabularx}
\bigskip

\end{center}
\clearpage

\vspace*{4cm}
\toelichting
\clearpage
}



\def\leesfile#1{%
\begin{database}{#1}{;}
  % whoswho csv dataformaat (eerste regel resultaat.csv)
{voornaam;voorletters;achternaam;geslacht;mailinglist;straat;huisnummer;postcode;woonplaats;geboortedatum;email;osirisfoto;homepage;lidnr;toestand;tussenvoegsels;nickname;icq;publish;boekenmail;geboortenamen;mentorgroepnr;titels_prefix;titels_postfix;vakid_opsturen;stagelist;opmerkingen;studentnr;msn;jabber;lidvan;lidtot;in_almanak;teltypeA;telnrA;teltypeB;telnrB;teltypeC;telnrC;teltypeD;telnrD;gpg_keyid;is_masterlid;planet_rss;wie;wanneer;foto;studie}
\formulier
\end{database}
}

% vim: syntax=tex
