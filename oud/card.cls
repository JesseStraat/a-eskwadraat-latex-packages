% Copyright (C) 2007-2015 TeXniCie A-Eskwadraat <hektex@a-eskwadraat.nl>
%
% This file is part of the A-Eskwadraat LaTeX collection and may be
% redistributed under the terms of version 2 of the GNU General Public License
% as published by the Free Software Foundation. See LICENSE file for details.
%
%% $Id: card.cls 733 2015-02-23 14:38:43Z aldo $

\NeedsTeXFormat{LaTeX2e}
\newcommand{\card@classname}{card}
\ProvidesClass{\card@classname}[2009/09/24 v1.1 (Sweitse)]

%%% CLASS %%%

\LoadClass[10pt]{article}


%%% PACKAGES %%%

\RequirePackage[misc,alpine]{ifsym}
\RequirePackage[svgnames]{xcolor}
\RequirePackage{tikz,ifthen,calc}

% Requires the A--Eskwadraatlogo: aes2logo-rood.pdf
% UU design requires the wiggle: slinger.pdf
%% Background requires wiggle with 20% gray: slinger20.pdf
% Background requires white wiggle : slinger00.pdf


%%% SOME PRELIMINARY VARIABLES %%%

% Standard dimension of business card: 85mm x 55mm
\newlength{\card@basewidth}
\setlength{\card@basewidth}{85mm}
\newlength{\card@baseheight}
\setlength{\card@baseheight}{55mm}

% Number of columns / rows per page
\newcounter{card@hcount}
\newcounter{card@vcount}

% Spacing inbetween cards
\newlength{\card@hsep}
\newlength{\card@vsep}


%%% OPTIONS %%%

% For test purposes, just one card per page
\DeclareOption{test}{
  \pdfpagewidth=95mm
  \pdfpageheight=65mm
  \setcounter{card@hcount}{1}
  \setcounter{card@vcount}{1}
}
% A3 option, default
\DeclareOption{a3}{
  \pdfpagewidth=297mm
  \pdfpageheight=420mm
  \setcounter{card@hcount}{3}
  \setcounter{card@vcount}{7}
}
% A4 option
\DeclareOption{a4}{
  \pdfpagewidth=210mm
  \pdfpageheight=297mm
  \setcounter{card@hcount}{2}
  \setcounter{card@vcount}{5}
}
\ExecuteOptions{a3}
\ProcessOptions


%%% COLOR SETTINGS %%%

% Set color model to CMYK (see: xcolor.sty/pdf)
\selectcolormodel{cmyk}

% Color palette, specify in cmyk if possible.
\definecolor {card@primary}     {cmyk} {0.00, 0.91,  0.76, 0.06} % UU red
\definecolor {card@secondary}   {cmyk} {0.00, 0.115, 0.94, 0.15} % UU yellow
\colorlet    {card@tertiary}    {white!50!card@secondary}
\colorlet    {card@quarternary} {white}

%% USER COMMAND: Change base colors
%% #1: optional quarternary color, default is white
%% #2: primary color (red)
%% #3: secondary color (yellow)
%% #4: tertiary color (yellow!50!white)
\newcommand{\kleuren}[4][white]{%
  \colorlet{card@primary}{#2}%
  \colorlet{card@secondary}{#3}%
  \colorlet{card@tertiary}{#4}%
  \colorlet{card@quarternary}{#1}%
}


%%% FONT SETTINGS %%%

% Set roman (=> default) font family to Palatino
\renewcommand{\rmdefault}{ppl}


%%% DIMENSIONS %%%

\setlength{\card@hsep}{(\pdfpagewidth -\thecard@hcount\card@basewidth )/(\value{card@hcount}+1)}
\setlength{\card@vsep}{(\pdfpageheight-\thecard@vcount\card@baseheight)/(\value{card@vcount}+1)}

\setlength{\hoffset}			{-1in}
\setlength{\voffset}			{-1in}
\setlength{\headheight}		{0mm}
\setlength{\headsep}			{0mm}
\setlength{\topmargin}		{\card@vsep}
\setlength{\oddsidemargin}{\card@hsep}
\setlength{\textwidth}		{\pdfpagewidth -\card@hsep}
\setlength{\textheight}		{\pdfpageheight-\card@vsep}
\setlength{\parindent}		{0mm}


%%% LAYOUT %%%

\pagestyle{empty}
\pagecolor{gray!10}


%%% LOGO %%%

% Import logo A-Eskwadraat at the given size
\pgfdeclareimage[width=20mm]{card@logo}{aes2logo-rood}
\pgfdeclareimage[width=120mm]{card@doublewiggle}{slinger00}


%%% INTERNAL COMMANDS %%%

\newcommand{\card@tmpdrawcard}{}
\newcommand{\card@name}{}
\newcommand{\card@function}{}
\newcommand{\card@email}{}
\newcommand{\card@phone}{}
\newcommand{\card@aesnaam}{Studievereniging A--Eskwadraat}
\newcommand{\card@address}{Princetonplein 5\\3584 CC Utrecht\\\ \\ 030 253 4499\\www.A--Eskwadraat.nl}

\newcounter{card@hcardcount}
\newcounter{card@vcardcount}
\newcounter{card@loopcounter}

% Dimensions of textbox (cm)
\newcommand{\card@wwidth} {6.4}
\newcommand{\card@wheight}{4}

% Radius of base circle
\newcommand{\card@radius}{35mm}

% Set default layout
\newcommand{\drawcard}{\drawcardap}


%%% EXTERNAL COMMANDS %%%

%% USER COMMAND: maakkaartje
% #1: number of cards (optional)
% #2: name
% #3: function
% #4: e-mail
\newcommand{\maakkaartje}[5][1]{%
  \renewcommand{\card@name}{#2}%
  \renewcommand{\card@function}{#3}%
  \renewcommand{\card@email}{#4}%
  \renewcommand{\card@phone}{#5}
  \setcounter{card@loopcounter}{0}%
  \whiledo{\value{card@loopcounter}<#1}%
  {%
    \stepcounter{card@loopcounter}%
    % Draw a card:
    \drawcard%
    % Check for end of line (hcardcount=hcount) and end of page (vcardcount=vcount)
    \stepcounter{card@hcardcount}%
    \ifthenelse{\value{card@hcardcount}=\value{card@hcount}}%
    {%
      \stepcounter{card@vcardcount}%
      \ifthenelse{\value{card@vcardcount}=\value{card@vcount}}%
      {%
        \clearpage%
        \setcounter{card@vcardcount}{0}%
      }{%
        \\[\card@vsep]%
      }%
      \setcounter{card@hcardcount}{0}%
    }{%
    \hspace{\card@hsep}%
    }%
  }%
}

\newcommand{\maakachtergrond}[2][1]{%
  \let\card@tmpdrawcard\drawcard%
  \renewcommand{\drawcard}{\drawbackground{#2}}
  \maakkaartje[#1]{}{}{}{}%
  \let\drawcard\card@tmpdrawcard%
}

%% USER COMMAND: ontwerp
% #1: Layout "ap" or "uu"
% #2: Width of textbox (max)
% #3: Height of textbox (max)
% #4: Radius of base circle
\newcommand\ontwerp[4]{%
  \ifthenelse{\equal{#1}{ap}}%
  {%
    \renewcommand{\drawcard}{\drawcardap}
  }{%
    \ifthenelse{\equal{#1}{uu}}%
    {%
      \renewcommand{\drawcard}{\drawcarduu}
    }{%
      \ClassError{\card@classname: Chosen ontwerp invalid. Please choose between "uu" and "ap".}
    }%
  }%
  \renewcommand{\card@wwidth}{#2}%
  \renewcommand{\card@wheight}{#3}%
  \renewcommand{\card@radius}{#4}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% AP DESIGN %%%

\newcommand{\drawcardap}{%
\begin{tikzpicture}[style=ultra thick] % Set ultra thick lines by default

% Boundaries of raw tikz image
\newcommand{\card@bleft}{-1}
\newcommand{\card@bright}{9.5}
\newcommand{\card@bbottom}{-1}
\newcommand{\card@btop}{6.5}

% Clip the raw tikz image to card format
\clip(0,0) rectangle (8.5,5.5);

% Fill with background color (the color of the upper right corner)
\fill[card@secondary] (\card@bleft,\card@bbottom) rectangle (\card@bright,\card@btop);

% Create textblock
\fill[card@quarternary]
  (\card@bleft,\card@bbottom)
    -- (\card@bleft,\card@wheight) [xshift=-\card@radius]
    -- (\card@bright,\card@wheight) [xshift=\card@radius]
    arc (90:0:\card@radius)
    -- (\card@bright,\card@bbottom)
    -- cycle;

% Draw lines and sidebars
\path[draw=card@primary,fill=card@tertiary] 
  (\card@bleft,\card@btop) [xshift=-\card@radius]
    -- (\card@wwidth,\card@btop) [xshift=\card@radius] 
    arc (90:0:\card@radius)
    -- (\card@wwidth,\card@bbottom)
    -- (\card@bright,\card@bbottom) [yshift=-\card@radius]
    -- (\card@bright,\card@wheight) [yshift=\card@radius]
    arc(0:90:\card@radius)
    -- (\card@bleft,\card@wheight)
    -- cycle;

% Place logo at the given position
\draw (7.3,4.7) node {\pgfuseimage{card@logo}};

% Use the following margin to card border
\newcommand{\card@sidemargin}{0.1}

% Text header
\draw[xshift={\card@sidemargin cm},yshift={-\card@sidemargin cm}]
  (\card@bleft,\card@wheight)
    -- node[midway,above right,text=card@primary,xshift=1cm,yshift=-.75cm] {\card@aesnaam}
    (\card@bleft,\card@btop);

% Name and function
\draw[yshift=-1cm]
  % Name
  (\card@sidemargin,\card@wheight) node[right,font=\Large] {\card@name}
  % Function
  (\card@sidemargin,\card@wheight) node[right,yshift=-1.5\baselineskip,font=\footnotesize\itshape,card@primary] {\card@function};

\draw
  % E-mail
  (\card@sidemargin,\card@sidemargin) node[above right,font=\footnotesize] {\card@email}
  % Phone
  (\card@sidemargin,\card@sidemargin) node[above right,font=\footnotesize,yshift=1\baselineskip] {\card@phone}
  % Address
  (\card@wwidth,\card@sidemargin) node[above right,font=\tiny,card@primary,text width= 2cm] {\card@address};

\end{tikzpicture}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% UU DESIGN

\newcommand{\drawcarduu}{%
\pgfdeclareimage[width=\card@wwidth cm]{card@wiggle}{slinger}%
\begin{tikzpicture}[style=very thick]
\clip(0,0) rectangle (8.5,5.5);
\fill[card@quarternary] (-1,-1) rectangle (9.5,6.5);
\draw[card@primary,fill=card@secondary,yshift=-\card@radius] (4.25,\card@wheight) circle (\card@radius);
\draw[card@primary,xshift={\card@radius-\card@wwidth cm}] (8.5,2.75) circle (\card@radius);

\begin{scope}
\clip[yshift=-\card@radius] (4.25,\card@wheight) circle (\card@radius);
\clip[xshift={\card@radius-\card@wwidth cm}] (8.5,2.75) circle (\card@radius);
\draw[opacity=.25] (8.5,0.1) node[left] {\pgfuseimage{card@wiggle}};
\end{scope}

\draw[yshift=.5cm] (0.1,\card@wheight) node[above right,scale=1.2] {\pgfuseimage{card@logo}};

\draw[yshift=-1.5cm,xshift=2mm-\card@wwidth cm]
  (8.5,5.5) node[above right,font=\Large,card@primary] {\card@name} [yshift=-1.5\baselineskip]
  (8.5,5.5) node[above right,card@primary] {\card@function};

\draw[xshift=2mm-\card@wwidth cm]
  (8.5,0) node[above right,font=\small] {\parbox{8.5cm}{%
    \begin{tabular}{cl}
    \Letter & Princetonplein 5\\ & 3584 CC Utrecht\\
    \Telephone & 030 253 4499\\
    \PaperLandscape & \card@email\\
    \Tent & www.A--Eskwadraat.nl
    \end{tabular}
    }};

\end{tikzpicture}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newcommand{\drawbackground}[1]{%
\begin{tikzpicture}

% Clip the raw tikz image to card format
\clip(0,0) rectangle (8.5,5.5);

% Boundaries of raw tikz image
\newcommand{\card@bleft}{-1}
\newcommand{\card@bright}{9.5}
\newcommand{\card@bbottom}{-1}
\newcommand{\card@btop}{6.5}

% Fill with background color (the color of the upper right corner)
\fill[#1] (\card@bleft,\card@bbottom) rectangle (\card@bright,\card@btop);

% Place logo at the given position
\draw (6.5,9) node {\pgfuseimage{card@doublewiggle}};

\end{tikzpicture}%
}
